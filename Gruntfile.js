module.exports = function(grunt) {

	grunt.initConfig({
		clean: {
			coverage: {
				src: ['coverage/']
			}
		},
		copy: {
			coverage: {
				src: ['test/**'],
				dest: 'coverage/'
			}
		},
		blanket: {
			coverage: {
				src: ['lib/'],
				dest: 'coverage/lib/'
			}
		},
		mochaTest: {
			options: {
				ui: 'tdd'
			},
			watch: {
				options: {
					reporter: 'min'
				},
				src: ['test/**/*_test.js']
			},
			test: {
				options: {
					reporter: 'nyan'
				},
				src: ['test/**/*_test.js']
			},
			cvTest: {
				options: {
					reporter: 'spec'
				},
				src: ['coverage/test/**/*_test.js']
			},
			coverage: {
				options: {
					reporter: 'html-cov',
					quiet: true,
					captureFile: 'coverage/coverage.html'
				},
				src: ['coverage/test/**/*_test.js']
			}
		},
		watch: {
			lib: {
				files: ['lib/**/*.js'],
				tasks: ['mochaTest:watch']
			},
			test: {
				files: ['test/**/*_test.js'],
				tasks: ['mochaTest:watch']
			}
		}
	});

	// On watch events, if the changed file is a test file then configure mochaTest to only
	// run the tests from that file. Otherwise run all the tests
	var defaultTestSrc = grunt.config('mochaTest.test.src');
	grunt.event.on('watch', function(action, filepath) {
		grunt.config('mochaTest.test.src', defaultTestSrc);
		if (filepath.match('test/')) {
			grunt.config('mochaTest.test.src', filepath);
		}
	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask("default", ['test', 'watch']);
	grunt.registerTask("test", ['mochaTest:test']);
	grunt.registerTask("coverage", ['clean', 'blanket', 'copy', 'mochaTest:cvTest', 'mochaTest:coverage']);
	grunt.registerTask("jenkins", ['coverage']);

};

