"use strict";

var fs = require('fs'),
	ConfigException = require('./configexception.js');

function ConfigReader(spec) {
	var path = spec.path;
	if(!path) {
		path = process.env.HOME + "/.kodiremoterc";
	}

	var read = function() {
		if(fs.existsSync(path)) {
			return removeLastCharacter(readFile());
		}
		throw new ConfigException("Configurationfile '"+path+"' does not exist", 23);
	};

	function removeLastCharacter(string) {
		return string.slice(0, (string.length - 1));
	}
	function readFile() {
		return fs.readFileSync(path, { encoding: "UTF8" });
	}

	return Object.freeze({
		read: read
	});
}

module.exports = ConfigReader;

