
var assert = require("assert");
var Validator = require("../../lib/configuration/kodiremotercvalidator.js");

suite("Kodiremoterc Validity Checker", function() {

	test("config with at least one host with location with hostname is valid", function() {
		var kodiremoterc = {
			"host": {
				"location": {
					"hostname": "9.9.9.9"
				}
			}
		};
		configContent(JSON.stringify(kodiremoterc)).isValid(kodiremoterc);
	});
	test("config with no hostname should throw Config-Exception with errorCode 22", function() {
		var kodiremoterc = {
			"host": {
				"location": {}
			},
			"host2": {}
		};
		configContent(JSON.stringify(kodiremoterc)).shouldThrowConfigException().withErrorCode(22);
	});
	test('malformed json config should throw Config-Exception with errorCode 21', function() {
		configContent("{").shouldThrowConfigException().withErrorCode(21);
	});

	var configContent = function(kodiremoterc) {
		return {
			"isValid": function(expectedConfigObject) {
				assert.deepEqual(runValidate(kodiremoterc), expectedConfigObject);
			},
			"shouldThrowConfigException": function() {
				return {
					"withErrorCode": function(errorCode) {
						try {
							runValidate(kodiremoterc);
						} catch(e) {
							assert.strictEqual(e.errorCode, errorCode);
							return;
						}
						assert.ok(false, "assert.fail('Test failed!')");
					}
				};
			}
		};
	};
	var runValidate = function(kodiremoterc) {
		var validator= new Validator({kodiremoterc: kodiremoterc});
		return validator.validate();
	};
});
