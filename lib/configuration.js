"use strict";

var ConfigReader = require('./configuration/configreader.js');
var Validator = require('./configuration/kodiremotercvalidator.js');
var HostChooser = require('./configuration/hostchooser.js');
var UrlComposer = require('./configuration/urlcomposer.js');

var Configuration = function(spec) {
	var configpath = spec.configpath,
		name = spec.kodiname;

	try {
		var configuration = readConfigurationFrom(configpath);
		var validatedConfiguration = validate(configuration);
		var host = chooseFrom(validatedConfiguration).by(name);
	} catch(e) {
		console.error(e.message);
		process.exit(e.errorCode);
	}

	var url = function() {
		return new UrlComposer({config: host}).compose();
	};

	function readConfigurationFrom(configpath) {
		return new ConfigReader({path: configpath}).read();
	}

	function validate(configuration) {
		return new Validator({kodiremoterc: configuration}).validate();
	}

	function chooseFrom(configuration) {
		return {
			"by": function(name) {
				return new HostChooser({kodiremoterc: configuration}).choose(name);
			}
		};
	}

	return Object.freeze({
		url: url
	});

};

module.exports = Configuration;

