'use strict';

var assert = require("assert");
var UrlComposer = require("../../lib/configuration/urlcomposer.js");

suite('UrlComposer#compose()', function() {
	test('minimal kodiremoterc', function() {
		var kodiremoterc = {
			"location": {
				"hostname": "10.0.13.37"
			}
		};
		hostDefinition(kodiremoterc).shouldComposeTo("http://10.0.13.37:8080/jsonrpc");
	});
	test('complete kodiremoterc', function() {
		var kodiremoterc = {
			"location": {
				"protocol": "https:",
				"hostname": "10.333.222.111",
				"port": "8282",
				"pathname": "/kodi/jsonrpc"
			}
		};
		hostDefinition(kodiremoterc).shouldComposeTo("https://10.333.222.111:8282/kodi/jsonrpc");
	});
	test('minimal kodiremoterc with authentication', function() {
		var kodiremoterc = {
			"location": {
				"hostname": "42.42.42.42"
			},
			"auth": {
				"user": "usr",
				"password": "pw"
			}
		};
		hostDefinition(kodiremoterc).shouldComposeTo("http://usr:pw@42.42.42.42:8080/jsonrpc");
	});
	var hostDefinition = function(kodiremoterc) {
		return {
			"shouldComposeTo": function(url) {
				var composer = new UrlComposer({config: kodiremoterc});
				var actual = composer.compose();
				assert.equal(actual, url);
			}
		};
	};
});

