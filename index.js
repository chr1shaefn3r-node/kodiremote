#!/usr/bin/env node

var cmd = require('commander'),
	pjson = require('./package.json'),
	Configuration = require('./lib/configuration.js'),
	Remote = require('./lib/remote.js');

cmd.version(pjson.version)
	.option('-v, --volume <n>', 'Set the volume to <n>', parseInt)
	.option('-i, --volume-increment', 'Increment the volume')
	.option('-d, --volume-decrement', 'Decrement the volume')
	.option('-m, --mute', 'Mute')
	.option('-p, --play-pause', 'Toggle between play and pause')
	.option('-s, --stop', 'Stop playback')
	.option('-e, --english', 'English')
	.option('-S, --shutdown', 'Shutdown kodi')
	.option('-k, --kodi <value>', 'Use kodi-host specified in ~/.kodiremoterc')
	.option('-o, --ok', 'Select')
	.option('-b, --back', 'Back')
	.option('-u, --up', 'Up')
	.option('-r, --down', 'Down')
	.option('-l, --left', 'Left')
	.option('-g, --right', 'Right')
	.parse(process.argv);

var config = new Configuration({name: cmd.kodi}),
	remote = new Remote(config.url());

if (cmd.volume) {
	remote.volume(cmd.volume);
}

if(cmd.playPause) {
	remote.playPause();
}

if(cmd.volumeIncrement) {
	remote.volumeIncrement();
}

if(cmd.volumeDecrement) {
	remote.volumeDecrement();
}

if(cmd.stop) {
	remote.stop();
}

if(cmd.mute) {
	remote.mute();
}

if(cmd.shutdown) {
	remote.shutdown();
}

if(cmd.english) {
	remote.nextAudioStream();
	remote.nextSubtitle();
}

if(cmd.ok) {
	remote.ok();
}

if(cmd.back) {
	remote.back();
}

if(cmd.down) {
	remote.down();
}

if(cmd.up) {
	remote.up();
}

if(cmd.left) {
	remote.left()
}

if(cmd.right) {
	remote.right();
}

