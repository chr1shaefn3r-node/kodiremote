# kodiremote

>  A command-line tool to remote control your kodi installation

## CLI

```sh
$ npm install --global kodiremote
```

```sh
$ kodiremote --help

  Usage: kodiremote [options]

  Options:

    -h, --help              output usage information
    -V, --version           output the version number
    -v, --volume <n>        Set the volume to <n>
    -i, --volume-increment  Increment the volume
    -d, --volume-decrement  Decrement the volume
    -m, --mute              Mute
    -p, --play-pause        Toggle between play and pause
    -s, --stop              Stop playback
    -e, --english           English
    -S, --shutdown          Shutdown kodi
    -k, --kodi <value>      Use kodi-host specified in ~/.kodiremoterc
    -o, --ok                Select
    -b, --back              Back
    -u, --up                Up
    -r, --down              Down
    -l, --left              Left
    -g, --right             Right
```

## Config

Stored in ~/.kodiremoterc

```json
{
	"<kodi-host>": {
		"location": {
			"hostname": "<ip>"
		},
		"auth": {
			"user": "<username>",
			"password": "<secret_password>"
		}
	}
}
```

## License

MIT © [Christoph Häfner](https://christophhaefner.de)

