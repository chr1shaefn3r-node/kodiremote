var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

gulp.task('runMochaTestSpec', function () {
		return gulp.src('test/**/*_test.js', {read: false})
			.pipe(plugins.mocha({ui: 'tdd', reporter: 'spec'}));
});
gulp.task('runMochaTestMin', function () {
		return gulp.src('test/**/*_test.js', {read: false})
			.pipe(plugins.mocha({ui: 'tdd', reporter: 'min'}));
});

gulp.task('watchTests', function() {
	gulp.watch(['lib/**/*.js', 'test/**/*_test.js'], ['runMochaTestMin']);
});

gulp.task('default', ['runMochaTestSpec', 'watchTests']);
gulp.task('test', ['runMochaTestSpec']);

