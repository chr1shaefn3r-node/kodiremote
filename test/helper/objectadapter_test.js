"use strict";

var assert = require("assert");
var ObjectAdapter = require("../../lib/helper/objectadapter.js");

suite("Object Adapter", function() {
	suite("#forEach()", function() {
		test("get a callback", function(done) {
			var host = {
				"location": {
					"hostname": "10.0.13.37"
				}
			};
			var kodiremoterc = {
				"host": host
			};
			new ObjectAdapter({obj: kodiremoterc}).forEach(function(value) {
				assert.strictEqual(value, host);
				done();
			});
		});
	});
	suite("#some()", function() {
		test("one true", function() {
			var kodiremoterc = {
				"host": {
					"default": true
				}
			};
			var actual = new ObjectAdapter({obj: kodiremoterc}).some(function(value) {
				return value.default;
			});
			assert.ok(actual);
		});
		test("none true", function() {
			var kodiremoterc = {
				"host": {},
				"test": {}
			};
			var actual = new ObjectAdapter({obj: kodiremoterc}).some(function(value) {
				return value.default;
			});
			assert.strictEqual(actual, false);
		});
	});
});

