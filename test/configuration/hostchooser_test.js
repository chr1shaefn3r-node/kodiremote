'use strict';

var assert = require('assert');
var HostChooser = require('../../lib/configuration/hostchooser.js');

suite("HostChooser#choose()", function() {
	test("empty config and empty kodiname should return undefined", function() {
		config().shouldReturn(undefined);
	});
	test("return named hostdefinition", function() {
		var host = {
			"location": {
				"hostname": "8.8.8.8"
			}
		};
		var kodiremoterc = {
			"test1": host
		};
		config(kodiremoterc, "test1").shouldReturn(host);
	});
	test("return default hostdefinition", function() {
		var host = {
			"location": {
				"hostname": "8.8.8.8"
			},
			"default": true
		};
		var kodiremoterc = {
			"test1": host,
			"test2": {
				"makeItHaveSomeProperties": "toEnsureEqualsCanWorkProperly"
			}
		};
		config(kodiremoterc).shouldReturn(host);
	});
	test("return first hostdefinition", function() {
		var host = {
			"location": {
				"hostname": "8.8.8.8"
			}
		};
		var kodiremoterc = {
			"test1": host,
			"test2": {
				"makeItHaveSomeProperties": "toEnsureEqualsCanWorkProperly"
			}
		};
		config(kodiremoterc).shouldReturn(host);
	});

	var config = function(kodiremoterc, kodiname) {
		return {
			"shouldReturn": function(value) {
				var hostChooser = new HostChooser({kodiremoterc: kodiremoterc});
				var actual = hostChooser.choose(kodiname);
				assert.equal(actual, value);
			}
		};
	};
});

