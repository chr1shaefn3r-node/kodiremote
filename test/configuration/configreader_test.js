
var assert = require("assert");
var ConfigReader = require("../../lib/configuration/configreader.js");

suite("ConfigReader", function() {

	setup(function() {
		this._processEnvHome = process.env.HOME;
	});

	test("Read content of file", function() {
		read("test/contentTestFiles/test1").shouldGive("42");
	});
	test("Non existing file should throw exception with error code 23", function() {
		process.env.HOME = "test/contentTestFiles/notExisting";
		read().shouldThrowConfigException().withErrorCode(23);
	});
	test("Read default config", function() {
		process.env.HOME = "test/contentTestFiles";
		read().shouldGive("testContent");
	});

	teardown(function() {
		process.env.HOME = this._processEnvHome;
	});

	var read = function(path) {
		var configReader = new ConfigReader({path: path});
		return {
			"shouldGive": function(expectedContent) {
				assert.strictEqual(configReader.read(), expectedContent);
			},
			"shouldThrowConfigException": function() {
				return {
					"withErrorCode": function(expectedErrorCode) {
						try {
							configReader.read();
							assert.strictEqual(true, false, "assert.fail");
						} catch(e) {
							assert.strictEqual(e.errorCode, expectedErrorCode);
						}
					}
				};
			}
		};
	};
});

