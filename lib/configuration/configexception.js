"use strict";

var ConfigException = function(message, errorCode) {
	this.message = message;
	this.errorCode = errorCode;
};

module.exports = ConfigException;

