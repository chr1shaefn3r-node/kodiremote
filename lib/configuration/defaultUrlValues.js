'use strict';

var DefaultUrlValues = function(spec) {

	var protocol = function(protocol) {
		if(protocol) {
			return lastCharacter(protocol) === ':' ? protocol : protocol+":";
		}
		return "http:";
	};

	var port = function(port) {
		return port || "8080";
	};

	var pathname = function(pathname) {
		if(pathname) {
			return firstCharacter(pathname) === '/' ? pathname : "/"+pathname;
		}
		return "/jsonrpc";
	};

	function firstCharacter(string) {
		return string.slice(0, 1);
	}
	function lastCharacter(string) {
		return string.slice(-1);
	}

	return Object.freeze({
		protocol: protocol,
		port: port,
		pathname: pathname
	});
};

module.exports = DefaultUrlValues;

