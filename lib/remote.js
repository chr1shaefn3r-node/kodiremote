"use strict";

var request = require('request');

var Remote = function(url) {
	this.url = url;
};

Remote.prototype.url = undefined;

Remote.prototype.playPause = function() {
	this._callMethod("Player.PlayPause", {"playerid": 1});
};
Remote.prototype.stop = function() {
	this._callMethod("Player.Stop", {"playerid": 1});
};

Remote.prototype.volume = function(volume) {
	this._callMethod("Application.SetVolume", {"volume":volume});
};
Remote.prototype.volumeIncrement = function() {
	this._callMethod("Application.SetVolume", {"volume":"increment"});
};
Remote.prototype.volumeDecrement = function() {
	this._callMethod("Application.SetVolume", {"volume":"decrement"});
};
Remote.prototype.mute = function() {
	this._callMethod("Application.SetMute", {"mute":"toggle"});
};


Remote.prototype.ok = function() {
	this._callMethod("Input.Select");
};
Remote.prototype.back = function() {
	this._callMethod("Input.Back");
};
Remote.prototype.down = function() {
	this._callMethod("Input.Down");
};
Remote.prototype.up = function() {
	this._callMethod("Input.Up");
};
Remote.prototype.left = function() {
	this._callMethod("Input.Left");
};
Remote.prototype.right = function() {
	this._callMethod("Input.Right");
};

Remote.prototype.nextAudioStream = function() {
	this._callMethod("Player.SetAudioStream",
			{"playerid": 1, "stream": "next"});
};
Remote.prototype.nextSubtitle = function() {
	this._callMethod("Player.SetSubtitle",
			{"playerid": 1, "subtitle": "next"});
};

Remote.prototype.shutdown = function() {
	this._callMethod("System.Shutdown");
};

Remote.prototype._callMethod = function(method, params) {
	var body = {
		"jsonrpc": "2.0",
		"method": method,
		"id": 1
	};
	if(params) {
		body.params = params;
	}
	this._jsonCall(method, JSON.stringify(body));
};

Remote.prototype._jsonCall = function(method, body) {
	request.post({
		url: this.url+"?"+method,
		body: body
	}, function(err, resp, body) {
		if(err) console.log("Error: "+err);
		//if(resp) console.log("Response: "+JSON.stringify(resp));
		if(body) console.log("Body: "+body);
	});
};

module.exports = Remote;

