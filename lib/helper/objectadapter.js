"use strict";

var ObjectAdapter = function (spec) {
	var obj = spec.obj;

	var forEach = function (handler, bind) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				if (bind === undefined) {
					handler(obj[key], key, obj);
					continue;
				}

				handler.call(bind, obj[key], key, obj);
			}
		}
	};

	var some = function (handler, bind) {
		var iterationResult;
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				iterationResult  = bind === undefined ? handler(obj[key], key, obj) : handler.call(bind, obj[key], key, obj);

				if (iterationResult) {
					return true;
				}
			}
			return false;
		}
	};

	var getDefault = function() {
		return forEach(function(value) {
			if(true === value["default"]) {
				return value;
			}
		});
	};

	var getFirst = function() {
		for(var key in obj) {
			return obj[key];
		}
	};

	return Object.freeze({
		forEach: forEach,
		some: some,
		getDefault: getDefault,
		getFirst: getFirst
	});
}

module.exports = ObjectAdapter;

