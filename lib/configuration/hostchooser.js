"use strict";

var ObjectAdapter = require("../helper/objectadapter.js");

var HostChooser = function(spec) {
	var config = spec.kodiremoterc;

	var choose = function(kodiname) {
		if(config) {
			return chooseWithConfig(config, kodiname);
		}
		return;
	};

	function chooseWithConfig(config, kodiname) {
		if(kodiname) {
			return config[kodiname];
		}
		return chooseWithoutKodiname(config);
	}
	function chooseWithoutKodiname(config) {
		var objectAdapter = new ObjectAdapter({obj: config});
		var host = objectAdapter.getDefault();
		if(!host) {
			host = objectAdapter.getFirst();
		}
		return host;
	}


	return Object.freeze({
		choose: choose
	});
};

module.exports = HostChooser;

