'use strict';

var DefaultUrlValues = require('./defaultUrlValues.js'),
	util = require('util');

var UrlComposer = function(spec) {
	var config = spec.config;

	var compose = function() {
		var df = new DefaultUrlValues();
		var location = config.location;

		var protocol = df.protocol(location.protocol);
		var hostname = location.hostname;
		var port = df.port(location.port);
		var pathname = df.pathname(location.pathname);

		var auth = "";
		if(config.auth) {
			var user = config.auth.user;
			var password = config.auth.password;
			auth = util.format("%s:%s@", user, password);
		}

		return util.format("%s//%s%s:%s%s",
				protocol, auth, hostname, port, pathname);
	};

	return Object.freeze({
		compose: compose
	});
}

module.exports = UrlComposer;

