'use strict';

var assert = require("assert");
var DefaultUrlValues = require("../../lib/configuration/defaultUrlValues.js");

suite('Default', function() {
	setup(function() {
		this._default = new DefaultUrlValues();
	});
	suite('protocol is http', function() {
		test('none', function() {
			assert.equal("http:", this._default.protocol());
		});
		test('parameter - https', function() {
			assert.equal("https:", this._default.protocol('https:'));
		});
		test('Correct colon', function() {
			assert.equal("https:", this._default.protocol('https'));
		});
	});
	suite('port is 8080', function() {
		test('none', function() {
			assert.equal("8080", this._default.port());
		});
		test('parameter - 9000', function() {
			assert.equal("9000", this._default.port('9000'));
		});
	});
	suite('pathname is /jsonrpc', function() {
		test('none', function() {
			assert.equal("/jsonrpc", this._default.pathname());
		});
		test('parameter - /json', function() {
			assert.equal("/json", this._default.pathname('/json'));
		});
		test('parameter - slash', function() {
			assert.equal("/json", this._default.pathname('json'));
		});
	});
});

