
var assert = require("assert");
var Configuration = require("../lib/configuration.js");

suite("Configuration", function() {

	test("Success", function() {
		configuration("test/contentTestFiles/test2")
			.withHost("testHost")
			.shouldComposeToUrl("http://9.9.9.9:8080/jsonrpc");
	});

	var configuration = function(path) {
		return {
			"withHost": function(host) {
				return {
					"shouldComposeToUrl": function(url) {
						var configuration = new Configuration({configpath: path, kodiname: host});
						assert.strictEqual(configuration.url(), url);
					}
				};
			}
		};
	};
});

