"use strict";

var ObjectAdapter = require("../helper/objectadapter.js"),
	ConfigException = require("./configexception.js");

var ConfigurationValidator = function(spec) {
	var stringConfig = spec.kodiremoterc,
		config = undefined;

	var validate = function() {
		parseConfig();
		return oneHostWithValidHostdefinition();
	};

	function parseConfig() {
		try {
			config = JSON.parse(stringConfig);
		} catch(e) {
			throw new ConfigException("Config is not valid JSON.", 21);
		}
	}
	function oneHostWithValidHostdefinition() {
		var hasOne = hasOneHostWithLocationAndHostname();
		if(true === hasOne) {
			return config;
		}
		throw new ConfigException("Could not find a single host with location.hostname defined.", 22);

	}
	function hasOneHostWithLocationAndHostname() {
		return new ObjectAdapter({obj: config}).some(function(element) {
			return (element.location && element.location.hostname);
		});
	}

	return Object.freeze({
		validate: validate
	});
};

module.exports = ConfigurationValidator;

